/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.IConfig;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author philip
 */
public class Config  {
    public Config() {
        password = "root";
        playlist = new ArrayList<>();
        port = 8998;
        
        try {
            Sender sender = new Sender();
            sender.senderName = "SRF3";
            sender.setUrl(new URL("http://stream.srg-ssr.ch/m/drs3/mp3_128"));
            
            Sender sender2 = new Sender();
            sender2.senderName = "SRF3";
            sender2.setUrl(new URL("http://ice36.infomaniak.ch:8000/neo1"));
            
            playlist.add(sender);
            playlist.add(sender2);
        } catch (MalformedURLException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        speakerName = "Master";
    }
    
    public String password;
    public ArrayList<Sender> playlist;
    public int port;
    public int servicePort;
    public String speakerName;
    public IConfig configInterface;
    public ArrayList<AlarmClock> alarms;
}
