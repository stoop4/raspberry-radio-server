/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author philip
 */
public class Sender {
    public String senderName;
    private String senderURL;
    
    public URL getUrl() {
        try {
            return new URL(senderURL);
        } catch (MalformedURLException ex) {
            Logger.getLogger(Sender.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public void setUrl(URL url) {
        senderURL = url.toString();
    }
}
