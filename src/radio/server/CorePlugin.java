/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radio.server;

import interfaces.Command;
import interfaces.ICommand;
import interfaces.ISend;
import interfaces.PluginBase;
import model.Config;
import model.Message;
import org.java_websocket.WebSocket;

/**
 *
 * @author philip
 */
public class CorePlugin extends PluginBase {
    public CorePlugin(Config config, ISend send) {
        super(config, send);
        Commands.add(new Command("update-password", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                config.password = data;
                config.configInterface.Save();
                
                send.SendTo(conn, new Message("update-password", data));
            }

            @Override
            public void OnNewConnection(WebSocket conn) {
                send.SendTo(conn, new Message("update-password", config.password));
            }
        }));
    }
}
