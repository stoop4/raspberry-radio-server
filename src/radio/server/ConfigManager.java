/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radio.server;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Config;

/**
 *
 * @author philip
 */
public class ConfigManager {
    public static Config LoadOrNew() {
        
        Config config = null;
        
        File f = new File("config.json");
        if (f.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(f));
                StringBuilder sb = new StringBuilder();
                String sCurrentLine;
                while ((sCurrentLine = br.readLine()) != null) {
                    sb.append(sCurrentLine);
                }

               config = new Gson().fromJson(sb.toString(), Config.class);

            } catch (FileNotFoundException ex) {
                Logger.getLogger(RadioServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(RadioServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(RadioServer.class.getName()).log(Level.SEVERE, null, ex);
            }

            config = new Config();
        }
        
        return (config == null? new Config() : config);
    }
    
    public static void Save(Config config) {
        try {
            File f = new File("config.json");
            
            if(!f.exists()) {
                try {
                    f.createNewFile();
                } catch (IOException ex) {
                    Logger.getLogger(ConfigManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            BufferedWriter writer = new BufferedWriter(new FileWriter(f));
            writer.write(new Gson().toJson(config, Config.class));
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ConfigManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
