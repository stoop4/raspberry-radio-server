/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radio.server;

import alarm.clock.Alarmclock;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import interfaces.Command;
import interfaces.IConfig;
import interfaces.ISend;
import interfaces.PluginBase;
import model.Config;
import model.Message;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import radio.player.Radioplayer;

/**
 *
 * @author philip
 */
public class RadioServer extends WebSocketServer {

    private final List<WebSocket> auth_clients;

    private final Config config;
    private final List<PluginBase> plugins;
    private final List<Command> commands;
    private final ISend send;

    public RadioServer(InetSocketAddress address) {
        super(address);
        this.plugins = new ArrayList<>();
        this.auth_clients = new ArrayList<>();
        this.commands = new ArrayList<>();

        this.config = ConfigManager.LoadOrNew();

        ConfigManager.Save(config);

        send = new ISend() {

            @Override
            public void SendToAll(Message message) {
                for (WebSocket socket : auth_clients) {
                    socket.send(new Gson().toJson(message));
                }
            }

            @Override
            public void SendTo(WebSocket conn, Message message) {
                conn.send(new Gson().toJson(message));
            }

            @Override
            public void SendToLocalhost(Message message) {
                
                InvokeCommand(message, null);
            }
        };

        this.config.configInterface = new IConfig() {

            @Override
            public void Save() {
                ConfigManager.Save(config);
            }
        };

        Radioplayer radPlayer = new Radioplayer(config, send);

        this.AddPlugin(radPlayer);
        this.AddPlugin(new CorePlugin(config, send));
        this.AddPlugin(new Alarmclock(config, send ));

    }

    private void AddPlugin(PluginBase plugin) {

        plugins.add(plugin);
        plugin.Commands.forEach((el) -> {
            this.commands.add(el);
        });
    }
    
    private void InvokeCommand(Message message, WebSocket conn) {
        if (message.Message.equals("password")) {
            if (message.Data.equals(config.password)) {
                auth_clients.add(conn);
                for (Command c : commands) {
                    c.Action.OnNewConnection(conn);
                }
            } else {
                send.SendTo(conn, new Message("password", ""));
            }
        } else {
            commands.forEach((Command el) -> {
                if (el.Command.equals(message.Message)) {
                    el.Action.OnInvoke(conn, message.Data);
                }
            });
        }
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        send.SendTo(conn, new Message("password", ""));
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        auth_clients.remove(conn);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        System.out.println(message);
        Message msg = new Gson().fromJson(message, Message.class);
        InvokeCommand(msg, conn);
        
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        System.err.println("an error occured on connection " + conn.getRemoteSocketAddress() + ":" + ex);
    }
}
