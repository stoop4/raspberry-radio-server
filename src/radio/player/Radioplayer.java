/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radio.player;

import com.google.gson.Gson;
import interfaces.Command;
import interfaces.ICommand;
import interfaces.ISend;
import interfaces.PluginBase;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import lib.Audio;
import model.Config;
import model.Message;
import model.Sender;
import org.java_websocket.WebSocket;
import radio.server.RadioServer;

/**
 *
 * @author philip
 */
public class Radioplayer extends PluginBase {

    private List<Sender> senders;
    private Sender playing;
    private Thread playerThread;
    Player player;

    public Radioplayer(Config config, ISend send) {
        super(config, send);
        if (config == null) {
            this.senders = new ArrayList<>();

            Sender srf3 = new Sender();
            Sender neo1 = new Sender();

            srf3.senderName = "SRF3";
            neo1.senderName = "NEO1";

            try {
                srf3.setUrl(new URL("http://stream.srg-ssr.ch/m/drs3/mp3_128"));
                neo1.setUrl(new URL("http://ice36.infomaniak.ch:8000/neo1"));
            } catch (MalformedURLException ex) {
                Logger.getLogger(Radioplayer.class.getName()).log(Level.SEVERE, null, ex);
            }

            this.senders.add(neo1);
            this.senders.add(srf3);
        } else {
            this.senders = config.playlist;
        }

        Command play = new Command();
        play.Command = "play";
        play.Action = new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                play(0, send);
            }

            @Override
            public void OnNewConnection(WebSocket conn) {
                send.SendTo(conn, new Message("update-play-pause", playerThread == null ? "0" : "1"));
            }
        };

        Command pause = new Command();
        pause.Command = "pause";
        pause.Action = new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                stopPlaying(send);
            }

            @Override
            public void OnNewConnection(WebSocket conn) {

            }
        };

        Command playIndex = new Command();
        playIndex.Command = "play-index";
        playIndex.Action = new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                try {
                    int index = Integer.parseInt(data);
                    play(index, send);
                } catch (NumberFormatException e) {

                }
            }

            @Override
            public void OnNewConnection(WebSocket conn) {
                send.SendTo(conn, new Message("update-playlist-index", Integer.toString(senders.indexOf(playing))));

            }
        };

        Command volumeUp = new Command();
        volumeUp.Command = "volume-up";
        volumeUp.Action = new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                float newvol = Audio.getMasterOutputVolume() + 0.04f;
                Audio.setMasterOutputVolume(newvol > 1 ? 1 : newvol);
            }

            @Override
            public void OnNewConnection(WebSocket conn) {

            }
        };

        Commands.add(play);
        Commands.add(pause);
        Commands.add(playIndex);
        Commands.add(volumeUp);

        Commands.add(new Command("volume-down", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                float newvol2 = Audio.getMasterOutputVolume() - 0.04f;
                lib.Audio.setMasterOutputVolume(newvol2 < 0 ? 0 : newvol2);
            }

            @Override
            public void OnNewConnection(WebSocket conn) {

            }
        }));

        Commands.add(new Command("next", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                if (playing != null) {
                    int index = senders.indexOf(playing);
                    play(index + 1, send);
                }
            }

            @Override
            public void OnNewConnection(WebSocket conn) {

            }
        }));

        Commands.add(new Command("previous", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                if (playing != null) {
                    int index = senders.indexOf(playing);
                    play(index - 1, send);
                }
            }

            @Override
            public void OnNewConnection(WebSocket conn) {

            }
        }));

        Commands.add(new Command("get-sender-url", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                try {
                    int index = Integer.parseInt(data);
                    send.SendTo(conn, new Message("get-sender-url", senders.get(index).getUrl().toString()));
                } catch (Exception ex) {

                }
            }

            @Override
            public void OnNewConnection(WebSocket conn) {
            }
        }));

        Commands.add(new Command("update-speaker-name", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                config.speakerName = data;
                if (config.configInterface != null) {
                    config.configInterface.Save();
                }
                send.SendToAll(new Message("update-speaker-name", config.speakerName));
            }

            @Override
            public void OnNewConnection(WebSocket conn) {
                send.SendTo(conn, new Message("update-speaker-name", config.speakerName));
            }
        }));

        Commands.add(new Command("update-sender", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                String[] d = new Gson().fromJson(data, String[].class);
                try {
                    int index = Integer.parseInt(d[0]);
                    String name = d[1];
                    String url = d[2];
                    try {
                        if (index < senders.size() & index >= 0) {

                            senders.get(index).senderName = name;
                            senders.get(index).setUrl(new URL(url));
                        } else if (index == -2) {
                            Sender s = new Sender();
                            s.senderName = name;
                            s.setUrl(new URL(url));
                            senders.add(s);
                        }
                        List<String> sender = new ArrayList<>();
                        senders.forEach((el) -> {
                            sender.add(el.senderName);
                        });
                        send.SendToAll(new Message("update-playlist", new Gson().toJson(sender.toArray())));
                        if (config.configInterface != null) {
                            config.configInterface.Save();
                        }
                    } catch (MalformedURLException uex) {
                        System.out.println(uex.toString());
                    }
                } catch (Exception ex) {
                    System.out.println(ex.toString());
                }
            }

            @Override
            public void OnNewConnection(WebSocket conn) {
                List<String> sender = new ArrayList<>();
                senders.forEach((el) -> {
                    sender.add(el.senderName);
                });
                send.SendTo(conn, new Message("update-playlist", new Gson().toJson(sender.toArray())));
            }
        }));

        Commands.add(new Command("delete-sender", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                int index = Integer.parseInt(data);
                if (index < config.playlist.size()) {
                    config.playlist.remove(index);
                    config.configInterface.Save();

                    List<String> sender = new ArrayList<>();
                    senders.forEach((el) -> {
                        sender.add(el.senderName);
                    });
                    send.SendToAll(new Message("update-playlist", new Gson().toJson(sender.toArray())));
                }
            }

            @Override
            public void OnNewConnection(WebSocket conn) {
            }
        }));

    }

    //Playing Functions
    public void play(int index, ISend send) {
        int s = senders.size();
        //Too lazy for commenting out. And don't know wat ist's doing. 
        int max = index >= s ? s - 1 : index;
        final int i = max < 0 ? 0 : max;
        if (s != 0 && playing != senders.get(i)) {
            stopPlaying(send);
            URL url = senders.get(i).getUrl();
            if (url != null) {
                playerThread = new Thread(() -> {
                    try {
                        if (player != null) {
                            player.close();
                        }

                        URL playerURL = url;
                        player = new Player(playerURL.openStream());
                        playing = senders.get(i);

                        send.SendToAll(new Message("update-play-pause", "1"));
                        send.SendToAll(new Message("update-playlist-index", Integer.toString(i)));

                        player.play();
                    } catch (JavaLayerException | IOException ex) {
                        Logger.getLogger(RadioServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });

                playerThread.start();
            }
        } else {
        }
    }

    private void stopPlaying(ISend send) {
        if (playerThread != null) {
            if (player != null) {
                player.close();
            }
            playerThread.interrupt();
            playerThread = null;
        }
        send.SendToAll(new Message("update-play-pause", "0"));
        playing = null;
    }
}
