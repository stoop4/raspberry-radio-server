/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alarm.clock;

import com.google.gson.Gson;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import interfaces.Command;
import interfaces.ICommand;
import interfaces.ISend;
import interfaces.PluginBase;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Config;
import model.Message;
import model.AlarmClock;

import org.java_websocket.WebSocket;

/**
 *
 * @author philip
 */
public class Alarmclock extends PluginBase {

    private Config config;

    public Alarmclock(Config config, ISend send) {
        super(config, send);
        this.config = config;

        Commands.add(new Command("update-alarm-clock", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                String[] d = new Gson().fromJson(data, String[].class);
                int index = Integer.parseInt(d[0]);
                AlarmClock clock = new Gson().fromJson(d[1], AlarmClock.class);
                if (index >= 0 & index < config.alarms.size()) {
                    config.alarms.set(index, clock);
                    config.configInterface.Save();
                } else if (index == -2) {
                    config.alarms.add(clock);
                    config.configInterface.Save();
                }

                send.SendToAll(UpdateAlarmClock());
            }

            @Override
            public void OnNewConnection(WebSocket conn) {
                send.SendTo(conn, UpdateAlarmClock());
            }
        }));

        Commands.add(new Command("get-alarm-repeat", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                Message msg = new Message();
                int index = Integer.parseInt(data);
                msg.Message = "get-alarm-repeat";
                AlarmClock clock = config.alarms.get(index);
                boolean[] days = clock.GetDays();
                msg.Data = new Gson().toJson(days, boolean[].class);
                send.SendTo(conn, msg);
            }

            @Override
            public void OnNewConnection(WebSocket conn) {

            }
        }));

        Commands.add(new Command("get-alarm-radio", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                send.SendTo(conn, new Message("get-alarm-radio", Integer.toString(config.alarms.get(Integer.parseInt(data)).Radio)));
            }

            @Override
            public void OnNewConnection(WebSocket conn) {

            }
        }));

        Commands.add(new Command("delete-alarm", new ICommand() {

            @Override
            public void OnInvoke(WebSocket conn, String data) {
                int index = Integer.parseInt(data);
                config.alarms.remove(index);
                config.configInterface.Save();
                send.SendToAll(UpdateAlarmClock());
            }

            @Override
            public void OnNewConnection(WebSocket conn) {

            }
        }));

        Thread alarmThread = new Thread(() -> {

            List<Integer> timeIndex = new ArrayList<>();
            List<AlarmClock> recentTimer = new ArrayList<>();

            while (true) {
                // Initialize for every second
                AlarmClock[] alarms = new AlarmClock[config.alarms.size()];
                int counter = 0;
                Iterator<AlarmClock> iter = config.alarms.iterator();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                String time = sdf.format(Calendar.getInstance().getTime());
                int weekday = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

                //prepare weekday for moday first. and for Array
                weekday = weekday - 2;
                if (weekday < 0) {
                    weekday = weekday + 7;
                }

                while (iter.hasNext()) {
                    alarms[counter++] = iter.next();
                }

                for (AlarmClock alarm : alarms) {
                    if (alarm.Time.equals(time) & alarm.GetDays()[weekday]) {
                        if (alarm.Radio >= config.playlist.size() & recentTimer.indexOf(alarm) == -1) {
                            send.SendToLocalhost(new Message("pause", ""));
                        } else {
                            send.SendToLocalhost(new Message("play-index", Integer.toString(alarm.Radio)));
                        }

                        recentTimer.add(alarm);
                        timeIndex.add(60);
                    }
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Alarmclock.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                Iterator<Integer> iterTime = timeIndex.iterator();
                Iterator<AlarmClock> iterRecentTimer = recentTimer.iterator();
                int iterTimeCounter = 0;
                
                while(iterTime.hasNext() & iterRecentTimer.hasNext()) {
                    int t = iterTime.next();
                    AlarmClock c = iterRecentTimer.next();
                    
                    if(t <= 0) {
                        iterTime.remove();
                        iterRecentTimer.remove();
                    } else {
                        timeIndex.set(iterTimeCounter, t-1);
                    }
                    
                    iterTimeCounter++;
                }
            }

        });

        alarmThread.start();
    }

    private Message UpdateAlarmClock() {
        if (config.alarms == null) {
            config.alarms = new ArrayList<>();
        }
        Message msg = new Message();
        msg.Message = "update-alarm-clocks";
        msg.Data = new Gson().toJson(config.alarms.toArray(), AlarmClock[].class);

        return msg;
    }

}
