/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import org.java_websocket.WebSocket;

/**
 *
 * @author philip
 */
public interface ICommand {
    public void OnInvoke(WebSocket conn, String data);
    public void OnNewConnection(WebSocket conn);
}
