/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import model.Message;
import org.java_websocket.WebSocket;

/**
 *
 * @author philip
 */
public interface ISend {
    public void SendToAll(Message message);
    public void SendTo(WebSocket conn, Message message);
    public void SendToLocalhost(Message message);
    
}
