/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.ArrayList;
import java.util.List;
import model.Config;

/**
 *
 * @author philip
 */
public abstract class PluginBase  {
    public List<Command> Commands;
    protected Config config;
    protected ISend send;
    
    public PluginBase(Config config, ISend send) {
        Commands = new ArrayList<>();
        this.config = config;
        this.send = send;
    }
}
