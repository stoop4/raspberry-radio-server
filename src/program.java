
import java.net.InetSocketAddress;
import radio.server.ConfigManager;
import radio.server.RadioServer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author philip
 */
public class program {
 public static void main(String[] args) {
        int port = ConfigManager.LoadOrNew().port;
        RadioServer server = new RadioServer(new InetSocketAddress(port));
        server.run();

    }
}
